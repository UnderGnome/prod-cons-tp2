package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {

	Buffer buffer;
	
	@Before
	public void before() {
		buffer = new Buffer(5);
	}
	
    @Test
    public void testBuildSuccess() {
    	
        assertTrue(true);
    }

    @Test
    public void testgetCurrentLoad() {
    	assertEquals(buffer.getCurrentLoad(), 0);
    }
    
    @Test 
    public void testIsEmpty() {
    	assertEquals(buffer.isEmpty(), true);
    }
    
    @Test 
    public void testInsert() throws BufferFullException {
    	buffer.addElement(new Element(1));
    	buffer.addElement(new Element(2));
    	buffer.addElement(new Element(3));
    	assertEquals(buffer.getCurrentLoad(), 3);
    }
    
    @Test 
    public void testIsNotEmpty() throws BufferFullException {
    	buffer.addElement(new Element(1));
    	assertEquals(buffer.isEmpty(), false);
    }
    
    @Test 
    public void testToString() throws Exception {
    	buffer.addElement(new Element(1));
    	buffer.addElement(new Element(2));
    	buffer.addElement(new Element(3));
    	assertNotNull(buffer.toString());
    }
    
    @Test
    public void testRemoveElement() throws Exception {
    	buffer.addElement(new Element(1));
    	assertNotNull(buffer.removeElement());
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testRemoveElementEmptyBuffer() throws BufferEmptyException {
    	assertNotNull(buffer.removeElement());
    }
    
    @Test(expected = BufferFullException.class)
    public void testAddElementFullBuffer() throws BufferFullException {
    	buffer.addElement(new Element(1));
    	buffer.addElement(new Element(2));
    	buffer.addElement(new Element(3));
    	buffer.addElement(new Element(4));
    	buffer.addElement(new Element(5));
    	buffer.addElement(new Element(6));
    	assertEquals(buffer.getCurrentLoad(), 6);
    }
    
}


